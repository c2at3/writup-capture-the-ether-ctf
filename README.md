# WritUp Capture The Ether CTF

## Call Me

Source của Contract:
```solidity
pragma solidity ^0.4.21;

contract CallMeChallenge {
    bool public isComplete = false;

    function callme() public {
        isComplete = true;
    }
}
```

Để giải quyết bài này chỉ cần dùng `web3js` để gọi tới hàm `callme()` có trong hợp đồng:

```javascript
// khởi tạo web3
const Web3 = require('web3');

// ProviderInstance được tạo ra từ Infura: https://infura.io/
const providerInstance = "https://ropsten.infura.io/v3/8445096065ef445888158dc102b89a97";

var web3 = new Web3(new Web3.providers.HttpProvider(providerInstance));

// ABI của contract có thể tạo ra bằng cách compile(online/npm package) source của Contract,
// ở đây dùng http://remix.ethereum.org/#optimize=false&runs=200&evmVersion=null&version=soljson-v0.4.21+commit.dfe3193c.js
var abi_contract = [
    {
        "constant": false,
        "inputs": [],
        "name": "callme",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "isComplete",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    }
];

// địa chỉ của hợp đồng, được tạo ra thừ capturetheether
const myContractAddress = '0x09812076B6bF7CAf0Af970B1Faa679EAe535CAED';

const myContract = new web3.eth.Contract(abi_contract, myContractAddress);

// private key để xác định người dùng (ký giao dịch)
const private_key = 'private_key';
web3.eth.accounts.wallet.add(private_key);

const addressWallet = web3.eth.accounts.wallet[0].address;
// Lượng gas tối đa được cung cấp cho một giao dịch (Gas limit).
const gas = 300000;
// Giá gas trong wei để sử dụng cho các giao dịch.
const gasPrice = web3.utils.toWei('10', 'gwei');

myContract.methods.callme().send({
    from: web3.eth.accounts.wallet[0].address,
    gas: gas,
    gasPrice: gasPrice
}).then(console.log)
```

## Choose a nickname

Source của Contract:
```solidity
pragma solidity ^0.4.21;

// Relevant part of the CaptureTheEther contract.
contract CaptureTheEther {
    mapping (address => bytes32) public nicknameOf;

    function setNickname(bytes32 nickname) public {
        nicknameOf[msg.sender] = nickname;
    }
}

// Challenge contract. You don't need to do anything with this; it just verifies
// that you set a nickname for yourself.
contract NicknameChallenge {
    CaptureTheEther cte = CaptureTheEther(msg.sender);
    address player;

    // Your address gets passed in as a constructor parameter.
    function NicknameChallenge(address _player) public {
        player = _player;
    }

    // Check that the first character is not null.
    function isComplete() public view returns (bool) {
        return cte.nicknameOf(player)[0] != 0;
    }
}
```

Đề bài yêu cầu người dùng đổi được tên để hiển thị trên leaderboard.
để giải quyết bài này ta cần gọi tới hàm `setNickname()` của contract `CaptureTheEther`có địa chỉ đã được triển khai là **`0x71c46Ed333C35e4E6c62D32dc7C8F00D125b4fee`**.

*Chú ý hàm `setNickname` đang yêu cầu 1 parameter là `nickname` có kiểu dữ liệu là `bytes32`.*

Code giải bài này tương tự bài trên chỉ cần thay đổi `abi_contract` và hàm được gọi.

Đầu tiên là compile contract `CaptureTheEther` để lấy ABI và thực hiện lệnh gọi hàm `setNickname()`:

```javascript
const Web3 = require('web3');

const providerInstance = "https://ropsten.infura.io/v3/8445096065ef445888158dc102b89a97";

var web3 = new Web3(new Web3.providers.HttpProvider(providerInstance));

var abi_contract = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "nickname",
                "type": "bytes32"
            }
        ],
        "name": "setNickname",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "",
                "type": "address"
            }
        ],
        "name": "nicknameOf",
        "outputs": [
            {
                "name": "",
                "type": "bytes32"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    }
]

// Địa chỉ mà contract CaptureTheEther được triển khai
const myContractAddress = '0x71c46Ed333C35e4E6c62D32dc7C8F00D125b4fee';

const myContract = new web3.eth.Contract(abi_contract, myContractAddress);
const private_key = '=))';
web3.eth.accounts.wallet.add(private_key);

const addressWallet = web3.eth.accounts.wallet[0].address;
const gas = 300000;
const gasPrice = web3.utils.toWei('10', 'gwei');

// web3.utils.asciiToHex: convert Ascii thành Hex
myContract.methods.setNickname(web3.utils.asciiToHex('QuangTB')).send({
    from: web3.eth.accounts.wallet[0].address,
    gas: gas,
    gasPrice: gasPrice
}).then(console.log)
```

Sau khi yêu cầu set nickname được thực hiện thành công thì ta thực hiện lệnh gọi hàm `isComplete()` ở contract `NicknameChallenge` xem kí tự đầu tiên của nickname có bị rỗng không. Cuối cùng là check solution.

## Guess the number

Source contract:
```solidity
pragma solidity ^0.4.21;

contract GuessTheNumberChallenge {
    uint8 answer = 42;

    // Contrustor: dùng metamask khởi tạo challenges phát mất 1 ether + gas.
    function GuessTheNumberChallenge() public payable {
        require(msg.value == 1 ether);
    }

    function isComplete() public view returns (bool) {
        return address(this).balance == 0;
    }

    // gửi một con số (n), nếu số n = answer (42) thì thắng 2 ether
    function guess(uint8 n) public payable {
        // phải gửi con số ngẫu nhiên và đi kèm 1 ether nữa
        require(msg.value == 1 ether);

        if (n == answer) {
            msg.sender.transfer(2 ether);
        }
    }
}
```
Đề bài đoán số, view qua source code có thể thấy con số bí mật là `answer = 42`.

Giải quyết challenges này bằng cách gửi **1 ether** kèm theo **con số bí mật** đã tìm được trong source (42) để nhận về 2 ether (chiến thắng).

Code giải thử thách tương tự 2 bài đầu, nhưng khi gọi hàm bằng web3 phải gửi thêm 1 trường `value` (vì hàm `guess()` yêu cầu phải trả 1 ether khi gọi)

```javascript
const Web3 = require('web3');

const providerInstance = "https://ropsten.infura.io/v3/8445096065ef445888158dc102b89a97";

var web3 = new Web3(new Web3.providers.HttpProvider(providerInstance));

var abi_contract = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "n",
                "type": "uint8"
            }
        ],
        "name": "guess",
        "outputs": [],
        "payable": true,
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "isComplete",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "payable": true,
        "stateMutability": "payable",
        "type": "constructor"
    }
]

const myContractAddress = '0x58F6812Edb4521d54e5d38AC2C2fd8f20DFf466d';

const myContract = new web3.eth.Contract(abi_contract, myContractAddress);

const private_key = 'xxxx';
web3.eth.accounts.wallet.add(private_key);

const addressWallet = web3.eth.accounts.wallet[0].address;
const gas = 300000;
const gasPrice = web3.utils.toWei('10', 'gwei');

myContract.methods.guess(42).send({
    from: web3.eth.accounts.wallet[0].address,
    gas: gas,
    gasPrice: gasPrice,
    // trả 1 ether
    value: web3.utils.toWei('1', 'ether')
}).then(console.log)
```

## Guess the secret number

Tương tự như bài *Guess the number*, nhưng ở thử thách này, con số bí mật được hash bằng `keccak256`.

Để giải quyết bài này ta sẽ viết một đoạn mã javascript để bruteforce giá trị của `answerHash = 0xdb81b4d58595fbbbb592d3661a34cdca14d7ab379441400cbfa1b78bc447c365;`

```javascript
// BruteForce gía trị của answer
answerHash = "0xdb81b4d58595fbbbb592d3661a34cdca14d7ab379441400cbfa1b78bc447c365".replace(/0x/g, '');
let i = 0;
var answer_decode;
while(true) {
    var hex_int = keccak256(i).toString('hex');
    if (hex_int === answerHash) {
        answer_decode = i;
        console.log('Answer decode: ' + answer_decode);
        break;
    }
    i++;
}
```

Bài giải hoàn chỉnh:
```javascript
const Web3 = require('web3');
const keccak256 = require('keccak256')

const providerInstance = "https://ropsten.infura.io/v3/8445096065ef445888158dc102b89a97";

var web3 = new Web3(new Web3.providers.HttpProvider(providerInstance));

var abi_contract = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "n",
                "type": "uint8"
            }
        ],
        "name": "guess",
        "outputs": [],
        "payable": true,
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "isComplete",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "payable": true,
        "stateMutability": "payable",
        "type": "constructor"
    }
]

const myContractAddress = '0xC88611B9A4FAe9ffdAcC6FF579be2fC1b2524A78';

const myContract = new web3.eth.Contract(abi_contract, myContractAddress);

const private_key = '???';
web3.eth.accounts.wallet.add(private_key);

const addressWallet = web3.eth.accounts.wallet[0].address;
const gas = 300000;
const gasPrice = web3.utils.toWei('10', 'gwei');

// BruteForce gía trị của answer
answerHash = "0xdb81b4d58595fbbbb592d3661a34cdca14d7ab379441400cbfa1b78bc447c365".replace(/0x/g, '');
let i = 0;
var answer_decode;
while(true) {
    var hex_int = keccak256(i).toString('hex');
    if (hex_int === answerHash) {
        answer_decode = i;
        console.log('Answer decode: ' + answer_decode);
        break;
    }
    i++;
}

myContract.methods.guess(answer_decode).send({
    from: web3.eth.accounts.wallet[0].address,
    gas: gas,
    gasPrice: gasPrice,
    value: web3.utils.toWei('1', 'ether')
}).then(console.log)
```

## Guess the random number

Code của bài này tương tự bài trên tuy nhiên thì câu trả lời được random và lưu trữ trong biến `answer`.

>> Memory is like RAM of your code, which holds the data in it till the execution of your function, once the function execution is over it is removed.On the other hand storage is like the database which exists in the blockchain independent of the function execution.

Biến được khai báo trong solidity sẽ được lưu trữ vĩnh viễn trong contract nên ta có thể truy xuất nó thông qua web3js với hàm: 
```javascript
web3.eth.getStorageAt('0x2a11a51F7E90826A9d93621Bc0Df2ECBf676EF62', 0)
```

Trong đó:
- Địa chỉ của Contract: `0x2a11a51F7E90826A9d93621Bc0Df2ECBf676EF6`
- Vị trị của biến  `answer`: 0

Lời giải đầy đủ:
```javascript
const Web3 = require('web3');
const keccak256 = require('keccak256')

const providerInstance = "https://ropsten.infura.io/v3/8445096065ef445888158dc102b89a97";

var web3 = new Web3(new Web3.providers.HttpProvider(providerInstance));

var abi_contract = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "n",
                "type": "uint8"
            }
        ],
        "name": "guess",
        "outputs": [],
        "payable": true,
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "isComplete",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "payable": true,
        "stateMutability": "payable",
        "type": "constructor"
    }
]

const myContractAddress = '0x2a11a51F7E90826A9d93621Bc0Df2ECBf676EF62';

const myContract = new web3.eth.Contract(abi_contract, myContractAddress);
const private_key = ':v';
web3.eth.accounts.wallet.add(private_key);

const addressWallet = web3.eth.accounts.wallet[0].address;
const gas = 300000;
const gasPrice = web3.utils.toWei('10', 'gwei');

web3.eth.getStorageAt(myContractAddress, 0).then(answer => {
    console.log(answer);
    myContract.methods.guess(answer).send({
        from: web3.eth.accounts.wallet[0].address,
        gas: gas,
        gasPrice: gasPrice,
        value: web3.utils.toWei('1', 'ether')
    }).then(console.log)
});
```

